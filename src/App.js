import React from "react";
import Fruta from "./components/Fruta";

class App extends React.Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ];

  render() {
    return (
      <div className="App">
        <div id="all fruit names">
          <Fruta fruits={this.fruits.map((fruit) => fruit.name)} />
        </div>
        <div id="red fruit names">
          <Fruta
            fruits={this.fruits
              .filter((fruit) => {
                const { color } = fruit;
                return color === "red";
              })
              .map((fruit) => fruit.name)}
          />
        </div>
        <div id="total">
          {this.fruits.reduce((total, elemento) => {
            return (total += elemento.price);
          }, 0)}
        </div>
      </div>
    );
  }
}

export default App;
