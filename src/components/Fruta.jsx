import React from "react";

class Fruta extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  render() {
    return (
      <div>
        {this.props.fruits.map((fruit) => (
          <li>{fruit}</li>
        ))}
      </div>
    );
  }
}

export default Fruta;
